from setuptools import setup

package_name = 'py_apriltag'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='admin',
    maintainer_email='admin@todo.todo',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'parse_node = py_apriltag.parse_node:main',
            'draw_axis_tf = py_apriltag.draw_axis_tf:main',
            'draw_axis_april = py_apriltag.draw_axis_april:main'
        ],
    },
)
