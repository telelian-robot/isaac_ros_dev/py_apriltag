import math

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image

from isaac_ros_apriltag_interfaces.msg import AprilTagDetectionArray

from cv_bridge import CvBridge # Package to convert between ROS and OpenCV Images
import cv2


def draw_axis(img, pitch, yaw, roll, center_x, center_y, size):
  y_flip = 1
  x_flip = 1
  x1 = x_flip * size * (math.cos(yaw) * math.cos(roll)) + center_x
  y1 = y_flip * size * (math.cos(pitch) * math.sin(roll) + math.cos(roll) * math.sin(pitch) * math.sin(yaw)) + center_y
  x2 = x_flip * size * (-math.cos(yaw) * math.sin(roll)) + center_x
  y2 = y_flip * size * (math.cos(pitch) * math.cos(roll) - math.sin(pitch) * math.sin(yaw) * math.sin(roll)) + center_y
  x3 = x_flip * size * (math.sin(yaw)) + center_x
  y3 = y_flip * size * (-math.cos(yaw) * math.sin(pitch)) + center_y

  # img , 시작, 끝 , BGR
  cv2.line(img, (int(center_x), int(center_y)), (int(x1),int(y1)),(0,0,255),3)
  cv2.line(img, (int(center_x), int(center_y)), (int(x2),int(y2)),(0,255,0),3)
  cv2.line(img, (int(center_x), int(center_y)), (int(x3),int(y3)),(255,0,0),3)

  return img

def euler_from_quaternion(x, y, z, w):
  t0 = +2.0 * (w * x + y * z)
  t1 = +1.0 - 2.0 * (x * x + y * y)
  x = math.atan2(t0, t1)

  t2 = +2.0 * (w * y - z * x)
  t2 = +1.0 if t2 > +1.0 else t2
  t2 = -1.0 if t2 < -1.0 else t2
  y = math.asin(t2)

  t3 = +2.0 * (w * z + x * y)
  t4 = +1.0 - 2.0 * (y * y + z * z)
  z = math.atan2(t3, t4)

  return x, y, z # in radians


class ApriltagSubscriber(Node):

    def __init__(self):
        super().__init__('april_detecter')
        self.image_sub = self.create_subscription(
            Image,
            'image',
            self.image_sub_callback,
            10)
            
        self.subscription = self.create_subscription(
            AprilTagDetectionArray, 
            'tag_detections', 
            self.sub_callback,
            10)
        self.frame_width = 1280
        self.frame_hegit = 720
        self.br = CvBridge()
        self.tX = 0
        self.tY = 0
        self.tZ = 0
        self.rX = 0
        self.rY = 0
        self.rZ = 0
        self.rW = 0
        
    def detect_id(self, detections):
        for detect in detections:
            if detect.family =='tag36h11':
                self.get_logger().debug('april_id = "%s"' % detect.id)
                
                self.tX = (detect.pose.pose.pose.position.x +0.5) * self.frame_width
                self.tY = (detect.pose.pose.pose.position.y +0.5) * self.frame_hegit 
                self.tZ = detect.pose.pose.pose.position.z
                self.get_logger().debug(f'{self.tX}, {self.tY}, {self.tZ}')
                
                self.rX = detect.pose.pose.pose.orientation.x
                self.rY = detect.pose.pose.pose.orientation.y
                self.rZ = detect.pose.pose.pose.orientation.z
                self.rW = detect.pose.pose.pose.orientation.w
                return detect.id 
        return 'unkown'
    
    def sub_callback(self, submsg):
        april_id = self.detect_id(submsg.detections)
        #self.get_logger().info('family = "%s"' % submsg.detections.family)
        #self.get_logger().info('april_id = "%s"' % submsg.detections.id)
        
    
    def image_sub_callback(self, msg):
        self.get_logger().debug('Receiving video frame')

        current_frame = self.br.imgmsg_to_cv2(msg)
        cv2.cvtColor(current_frame, cv2.COLOR_BGR2RGB, dst=current_frame)
        #cv2.flip(current_frame, 1)
        #self.frame_hegit, self.frame_width = current_frame.shape[:2]
        _x, _y, _z = euler_from_quaternion(self.rX, self.rY, self.rZ, self.rW)

        draw_axis(current_frame, _x, _y, _z , self.tX, self.tY, 100 * (self.tZ))
        cv2.imshow("camera", current_frame)
        cv2.waitKey(1)

def main(args=None):
    rclpy.init(args=args)

    april_subscriber = ApriltagSubscriber()

    rclpy.spin(april_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    april_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
    

