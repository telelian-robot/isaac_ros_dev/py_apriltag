import math

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from tf2_msgs.msg import TFMessage
from time import time

from cv_bridge import CvBridge # Package to convert between ROS and OpenCV Images
import cv2

def draw_axis(img, pitch, yaw, roll, center_x, center_y, size):
  y_flip = 1
  x_flip = 1
  x1 = x_flip * size * (math.cos(yaw) * math.cos(roll)) + center_x
  y1 = y_flip * size * (math.cos(pitch) * math.sin(roll) + math.cos(roll) * math.sin(pitch) * math.sin(yaw)) + center_y
  x2 = x_flip * size * (-math.cos(yaw) * math.sin(roll)) + center_x
  y2 = y_flip * size * (math.cos(pitch) * math.cos(roll) - math.sin(pitch) * math.sin(yaw) * math.sin(roll)) + center_y
  x3 = x_flip * size * (math.sin(yaw)) + center_x
  y3 = y_flip * size * (-math.cos(yaw) * math.sin(pitch)) + center_y

  # img , 시작, 끝 , BGR
  cv2.line(img, (int(center_x), int(center_y)), (int(x1),int(y1)),(0,0,255),3)
  cv2.line(img, (int(center_x), int(center_y)), (int(x2),int(y2)),(0,255,0),3)
  cv2.line(img, (int(center_x), int(center_y)), (int(x3),int(y3)),(255,0,0),3)

  return img

def euler_from_quaternion(x, y, z, w):
  t0 = +2.0 * (w * x + y * z)
  t1 = +1.0 - 2.0 * (x * x + y * y)
  x = math.atan2(t0, t1)

  t2 = +2.0 * (w * y - z * x)
  t2 = +1.0 if t2 > +1.0 else t2
  t2 = -1.0 if t2 < -1.0 else t2
  y = math.asin(t2)

  t3 = +2.0 * (w * z + x * y)
  t4 = +1.0 - 2.0 * (y * y + z * z)
  z = math.atan2(t3, t4)

  return x, y, z # in radians
   
class ImageSubscriber(Node):
  def __init__(self):
    super().__init__('my_image_viwer')
    self.image_sub = self.create_subscription(
      Image,
      'image',
      self.image_sub_callback,
      10)

    self.tf_sub = self.create_subscription(
      TFMessage,
      'tf',
      self.tf_sub_callback,
      10)

    self.frame_width = 1280
    self.frame_height = 720
    self.image_sub
    self.tf_sub
    self.br = CvBridge()
    self.axis_scale = 100
    self.get_logger().info('Image Subscriber has been initialized')
    self.Pose = None

  def tf_sub_callback(self, msg):
    for tr in msg.transforms :
        translation = tr.transform.translation
        rotation = tr.transform.rotation
        if translation.x != 0 and translation.y !=0 and translation.z != 0:
            self.Pose = {
                'coord' : {
                  'x' : (translation.x + 0.5 ) * self.frame_width,
                  'y' : (translation.y + 0.5 ) * self.frame_height,
                  'z' : 1 - translation.z,
                },
                'rot' : {
                  'x' : rotation.x,
                  'y' : rotation.y,
                  'z' : rotation.z,
                  'w' : rotation.w
                }
            }

  def image_sub_callback(self, msg):
    self.get_logger().info('Receiving video frame')
    if self.Pose is None:
      return
    
    current_frame = self.br.imgmsg_to_cv2(msg)
    cv2.cvtColor(current_frame, cv2.COLOR_BGR2RGB, dst=current_frame)

    rx , ry, rz = euler_from_quaternion(self.Pose['rot']['x'],  self.Pose['rot']['y'],  self.Pose['rot']['z'],  self.Pose['rot']['w'])
    draw_axis(current_frame, rx , ry, rz , self.Pose['coord']['x'], self.Pose['coord']['y'], self.axis_scale * self.Pose['coord']['z'])

    cv2.imshow("camera", current_frame)
    cv2.waitKey(1)
  
def main(args=None):
    rclpy.init(args=args)
    node = ImageSubscriber()

    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.getlogger().info('Keyboard Interrupt (SIGINT)')
    except Exception as e:
        node.getlogger().error(f'Exception: {e}')

    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
    