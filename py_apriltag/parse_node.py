import math

import rclpy
from rclpy.node import Node
from std_msgs.msg import String
from geometry_msgs.msg import Pose

from isaac_ros_apriltag_interfaces.msg import AprilTagDetectionArray

class ApriltagParsingNode(Node):
    def __init__(self):
        super().__init__('apriltag_parser')
        self.subscription = self.create_subscription(
            AprilTagDetectionArray, 
            'tag_detections', 
            self.sub_callback,
            10)
        self.get_logger().info('Apriltag Parsing Node has been initialized')
        
    def sub_callback(self, submsg):
        for detect in submsg.detections:
            if detect.family =='tag36h11':
                self.get_logger().info('april_id = "%s"' % detect.id)
                
                self.coord = {
                    'x': detect.pose.pose.pose.position.x,
                    'y': detect.pose.pose.pose.position.y,
                    'z': detect.pose.pose.pose.position.z,
                }

                self.rot ={
                    'x': detect.pose.pose.pose.orientation.x,
                    'y': detect.pose.pose.pose.orientation.y,
                    'z': detect.pose.pose.pose.orientation.z,
                    'w': detect.pose.pose.pose.orientation.w,
                }

                self.get_logger().info(f'{self.coord=}')
                self.get_logger().info(f'{self.rot=}')
    
def main(args=None):
    rclpy.init(args=args)
    node = ApriltagParsingNode()

    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.getlogger().info('Keyboard Interrupt (SIGINT)')
    except Exception as e:
        node.getlogger().error(f'Exception: {e}')

    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
    

